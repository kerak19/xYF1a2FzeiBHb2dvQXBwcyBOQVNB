package rest

import (
	"errors"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/apod"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/service/mock"
)

func TestFetchAPODURLs(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	logger := log.Default()
	// Make logger quiet
	logger.SetOutput(io.Discard)

	t.Run("Success", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "localhost:9090/pictures?start_date=2021-09-25&end_date=2021-09-26", nil)

		s := mock.NewMockService(ctrl)

		startDateString := "2021-09-25"
		endDateString := "2021-09-26"
		startDate, err := time.Parse(apod.DateFormat, startDateString)
		if err != nil {
			t.Error(err)
		}
		endDate, err := time.Parse(apod.DateFormat, endDateString)
		if err != nil {
			t.Error(err)
		}

		ret := []string{"https://url1", "https://url2"}
		s.EXPECT().FetchAPODURLs(req.Context(), startDate, endDate).Return(ret, nil)

		r := Rest{
			svc:    s,
			logger: logger,
		}

		r.FetchAPODURLs(rr, req)

		result := rr.Result()
		expStatus := http.StatusOK
		if result.StatusCode != expStatus {
			t.Errorf("Exp: %v\nGot: %v\n", expStatus, result.StatusCode)
		}

		b, err := io.ReadAll(result.Body)
		if err != nil {
			t.Error(err)
		}

		exp := `{"urls":["https://url1","https://url2"]}`
		if string(b) != exp {
			t.Errorf("Exp: %v\nGot: %v\n", exp, string(b))
		}
	})

	t.Run("ErrorInvalidMethod", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "localhost:9090/pictures?start_date=2021-09-25&end_date=2021-09-26", nil)

		r := Rest{
			logger: logger,
		}

		r.FetchAPODURLs(rr, req)

		result := rr.Result()
		expStatus := http.StatusMethodNotAllowed
		if result.StatusCode != expStatus {
			t.Errorf("Exp: %v\nGot: %v\n", expStatus, result.StatusCode)
		}
	})

	t.Run("ErrorInvalidStartDate", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "localhost:9090/pictures?start_date=2021-09-25Invalid&end_date=2021-09-26", nil)

		r := Rest{
			logger: logger,
		}

		r.FetchAPODURLs(rr, req)

		result := rr.Result()
		expStatus := http.StatusBadRequest
		if result.StatusCode != expStatus {
			t.Errorf("Exp: %v\nGot: %v\n", expStatus, result.StatusCode)
		}

		b, err := io.ReadAll(result.Body)
		if err != nil {
			t.Error(err)
		}

		exp := `{"error":"Invalid start_date"}`
		if string(b) != exp {
			t.Errorf("Exp: %v\nGot: %v\n", exp, string(b))
		}
	})

	t.Run("ErrorInvalidEndDate", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "localhost:9090/pictures?start_date=2021-09-25&end_date=2021-09-26Invalid", nil)

		r := Rest{
			logger: logger,
		}

		r.FetchAPODURLs(rr, req)

		result := rr.Result()
		expStatus := http.StatusBadRequest
		if result.StatusCode != expStatus {
			t.Errorf("Exp: %v\nGot: %v\n", expStatus, result.StatusCode)
		}

		b, err := io.ReadAll(result.Body)
		if err != nil {
			t.Error(err)
		}

		exp := `{"error":"Invalid end_date"}`
		if string(b) != exp {
			t.Errorf("Exp: %v\nGot: %v\n", exp, string(b))
		}
	})

	t.Run("ErrorFetchAPODURLs", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "localhost:9090/pictures?start_date=2021-09-25&end_date=2021-09-26", nil)

		s := mock.NewMockService(ctrl)

		startDateString := "2021-09-25"
		endDateString := "2021-09-26"
		startDate, err := time.Parse(apod.DateFormat, startDateString)
		if err != nil {
			t.Error(err)
		}
		endDate, err := time.Parse(apod.DateFormat, endDateString)
		if err != nil {
			t.Error(err)
		}

		s.EXPECT().FetchAPODURLs(req.Context(), startDate, endDate).Return(nil, errors.New("error"))

		r := Rest{
			svc:    s,
			logger: logger,
		}

		r.FetchAPODURLs(rr, req)

		result := rr.Result()
		expStatus := http.StatusInternalServerError
		if result.StatusCode != expStatus {
			t.Errorf("Exp: %v\nGot: %v\n", expStatus, result.StatusCode)
		}

		b, err := io.ReadAll(result.Body)
		if err != nil {
			t.Error(err)
		}

		exp := `{"error":"Error while fetching APO URLs"}`
		if string(b) != exp {
			t.Errorf("Exp: %v\nGot: %v\n", exp, string(b))
		}
	})
}
