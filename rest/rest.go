package rest

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/apod"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/rest/response"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/service"
)

// Rest describes HTTP service that manages all HTTP requests
type Rest struct {
	svc    service.Service
	logger *log.Logger
}

// New returns new instance of Service
func New(log *log.Logger, apodSvc service.Service) Rest {
	return Rest{
		logger: log,
		svc:    apodSvc,
	}
}

// FetchAPODURLs is an endpoint that returns a list of URLS of APOD from NASA
func (h Rest) FetchAPODURLs(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	v := r.URL.Query()

	startDateString := v.Get("start_date")
	endDateString := v.Get("end_date")

	startDate, err := time.Parse(apod.DateFormat, startDateString)
	if err != nil {
		log.Printf("Error while parsing startDate (%s): %v\n", startDateString, err)
		response.Error{
			Error: "Invalid start_date",
			Code:  http.StatusBadRequest,
		}.Write(w)
		return
	}

	endDate, err := time.Parse(apod.DateFormat, endDateString)
	if err != nil {
		log.Printf("Error while parsing endDate (%s): %v\n", endDateString, err)
		response.Error{
			Error: "Invalid end_date",
			Code:  http.StatusBadRequest,
		}.Write(w)
		return
	}

	urls, err := h.svc.FetchAPODURLs(r.Context(), startDate, endDate)
	if err != nil {
		if err == service.ErrStartDateAfterEndDate {
			response.Error{
				Error: "start_date happens after end_date",
				Code:  http.StatusBadRequest,
			}.Write(w)
			return
		}
		log.Printf("Error while fetching APOD URLS: %v\n", err)
		response.Error{
			Error: "Error while fetching APO URLs",
			Code:  http.StatusInternalServerError,
		}.Write(w)
		return
	}

	resp := struct {
		URLs []string `json:"urls"`
	}{URLs: urls}

	w.WriteHeader(http.StatusOK)
	b, _ := json.Marshal(resp)
	w.Write(b)

	h.logger.Println("Request handled correctly")
}
