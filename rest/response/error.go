package response

import (
	"encoding/json"
	"net/http"
)

// Error describes an error request
type Error struct {
	Code  int    `json:"-"`
	Error string `json:"error"`
}

// Write writes error as the response
func (e Error) Write(w http.ResponseWriter) {
	w.WriteHeader(e.Code)
	b, _ := json.Marshal(e)
	w.Write(b)
}
