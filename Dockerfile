FROM golang:alpine3.14
WORKDIR /app
COPY . ./
RUN go mod download
RUN go build -o /apod
CMD [ "/apod" ]