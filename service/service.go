package service

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/apod"
)

//go:generate mockgen -destination=mock/service_mock.go -package=mock github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/service Service

// Service describes Service domain
type Service interface {
	FetchAPODURLs(ctx context.Context, startDate, endDate time.Time) ([]string, error)
}

// service is an implementation of Service
type service struct {
	apodClient apod.APODClient
	logger     *log.Logger
}

// New returns new instance of Service
func New(log *log.Logger, ac apod.APODClient) service {
	return service{
		logger:     log,
		apodClient: ac,
	}
}

// ErrStartDateAfterEndDate is returned when startDate is after endDate
var ErrStartDateAfterEndDate = errors.New("startDate happens after endDate")

// FetchAPODURLs returns a list of APOD URLs within provided date range
func (s service) FetchAPODURLs(ctx context.Context, startDate, endDate time.Time) ([]string, error) {
	if startDate.After(endDate) {
		log.Printf("Start_date (%s) happens after end_date (%s)\n", startDate, endDate)
		return nil, ErrStartDateAfterEndDate
	}

	return s.apodClient.FetchURLs(ctx, startDate, endDate)
}
