package service

import (
	"context"
	"errors"
	"io"
	"log"
	"reflect"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/apod/mock"
)

func TestFetchAPODURLs(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ctx := context.Background()

	logger := log.Default()
	// Make logger quiet
	logger.SetOutput(io.Discard)

	t.Run("Success", func(t *testing.T) {
		apod := mock.NewMockAPODClient(ctrl)
		s := service{
			apodClient: apod,
		}

		startTime := time.Now().AddDate(0, 0, -2)
		endTime := time.Now()

		ret := []string{"https://url1", "https://url2"}

		apod.EXPECT().FetchURLs(ctx, startTime, endTime).Return(ret, nil)

		urls, err := s.FetchAPODURLs(ctx, startTime, endTime)
		if err != nil {
			t.Error(err)
		}
		if len(urls) != 2 {
			t.Error("URLs should contain 2 elements")
		}

		if !reflect.DeepEqual(ret, urls) {
			t.Errorf("elements don't match\nExp: %v\nGot: %v\n", ret, urls)
		}
	})

	t.Run("ErrorAPODFail", func(t *testing.T) {
		apod := mock.NewMockAPODClient(ctrl)
		s := service{
			apodClient: apod,
		}

		startTime := time.Now().AddDate(0, 0, -2)
		endTime := time.Now()

		apod.EXPECT().FetchURLs(ctx, startTime, endTime).Return(nil, errors.New("error"))

		_, err := s.FetchAPODURLs(ctx, startTime, endTime)
		if err.Error() != "error" {
			t.Errorf("Exp: %v\nGot: %v\n", "error", err)
		}
	})

	t.Run("ErrorStartDateAfterEndDate", func(t *testing.T) {
		apod := mock.NewMockAPODClient(ctrl)
		s := service{
			apodClient: apod,
		}

		startTime := time.Now().AddDate(0, 0, 2)
		endTime := time.Now()

		_, err := s.FetchAPODURLs(ctx, startTime, endTime)
		if err != ErrStartDateAfterEndDate {
			t.Errorf("Exp: %v\nGot: %v\n", ErrStartDateAfterEndDate, err)
		}
	})
}
