package apod

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"time"
)

//go:generate mockgen -destination=mock/apod_mock.go -package=mock github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/apod APODClient

// APODClient is an abstraction of APODClient
type APODClient interface {
	FetchURLs(ctx context.Context, startTime, endTime time.Time) ([]string, error)
}

// HTTPClient is an abstraction of http.Client, allowing more flexible
// configuration
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// apodAPIURL is the URL of the endpoint of NASA APOD API
const apodAPIURL = "https://api.nasa.gov/planetary/apod"

// Client handles all calls to NASA APOD API
type Client struct {
	httpClient HTTPClient
	apiKey     string
	apodAPIURL url.URL
	// limiter is there to limit the amount of concurrent requests
	limiter chan struct{}
}

// New returns new instance of APOD client
func New(apiKey string, maxConcurrency int, opts ...func(*Client)) *Client {
	// The apodAPIURL is internal URL and is expected to be always valid
	u, _ := url.Parse(apodAPIURL)

	limiter := make(chan struct{}, maxConcurrency)

	return &Client{
		httpClient: &http.Client{},
		limiter:    limiter,
		apodAPIURL: *u,
		apiKey:     apiKey,
	}
}

// WithHTTPClient allows to set own HTTPClient
func WithHTTPClient(httpClient HTTPClient) func(*Client) {
	return func(c *Client) {
		c.httpClient = httpClient
	}
}

// APOD describes structure returned from request to APOD API
type APOD struct {
	URL string
}

// DateFormat is the expected date format in APOD request
const DateFormat = "2006-01-02"

// FetchURLs fetches URLs to all images of the day within provided date range.
// It is expected the startTime is before endTime
func (c Client) FetchURLs(ctx context.Context, startTime, endTime time.Time) ([]string, error) {
	query := url.Values{}
	query.Add("api_key", c.apiKey)
	query.Add("start_date", startTime.Format(DateFormat))
	query.Add("end_date", endTime.Format(DateFormat))

	url := c.apodAPIURL
	url.RawQuery = query.Encode()

	var apods []APOD

	// If the limiter is full, wait until another job finishes
	c.limiter <- struct{}{}

	err := c.do(ctx, http.MethodGet, url.String(), nil, &apods)
	if err != nil {
		return nil, err
	}

	// Release the limiter seat
	<-c.limiter

	apodURLs := make([]string, 0, len(apods))
	for _, v := range apods {
		apodURLs = append(apodURLs, v.URL)
	}

	return apodURLs, nil
}

// do is a small wrapper for the HTTP request part of the code
func (c Client) do(ctx context.Context, method, url string, body io.Reader, respDest interface{}) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return err
	}
	req = req.WithContext(ctx)

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(respDest)
}
