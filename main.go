package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/apod"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/rest"
	"github.com/kerak19/xYF1a2FzeiBHb2dvQXBwcyBOQVNB/service"
)

// Available environment variables
var (
	apiKey             string
	concurrentRequests int
	port               string
)

func init() {
	apiKey = os.Getenv("API_KEY")
	if apiKey == "" {
		apiKey = "IXxOl8y2jDtDJlLtnc5EC8MbJJ7rRwRQtJOFmrBY"
	}

	cr := os.Getenv("CONCURRENT_REQUESTS")
	if cr != "" {
		var err error
		concurrentRequests, err = strconv.Atoi(cr)
		if err != nil {
			panic("invalid concurrent requests amount: " + err.Error())
		}
	}
	if concurrentRequests == 0 {
		concurrentRequests = 5
	}

	port = os.Getenv("PORT")
	if port == "" {
		port = "32768"
	}
}

func main() {
	c := apod.New(apiKey, 1)

	logger := log.Default()
	logger.SetFlags(log.Llongfile)

	svc := service.New(logger, c)

	rest := rest.New(logger, svc)

	fmt.Println("App is running...")
	http.HandleFunc("/pictures", rest.FetchAPODURLs)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
