# NASA APOD URLs fetcher

### To run without Go
```sh
docker build -t apod .
docker run --env-file .env apod
```

### To run with Go
```sh
go run main.go
```

### To test
```sh
go test ./...
```

### Discussion
  - What if we were to change the NASA API to some other images provider?
    - New API client would need to be created. Generic interface could be introduced to ease future changes of API client(s), if anticipated.
  - What if, apart from using NASA API, we would want to have another microservice fetching urls from European Space Agency. How much code could be reused?
    - It depends on the ESA API. Some code, like making request, could be easily reused (with small modification). In case of many sources, a generic interface could be introduced to ease usage and addition of multiple clients.
  - What if we wanted to add some more query params to narrow down lists of urls - for example,
selecting only images taken by certain person. (field copyright in the API response)
    - It's a small amount of work. Some light validation would be necessary (check if not empty). Unfortunately, APOD API doesn't support `copyright` filter, so it'd need to be done on the app side.
